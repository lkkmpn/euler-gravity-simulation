from dataclasses import dataclass


@dataclass
class Frame:
    t: float
    v: float
    y: float
