import argparse

import matplotlib

from .frame import Frame
from .simulation import Simulation
from .simulation_settings import SimulationSettings
from .visualization import gif_frames, plot_frames


def main():
    matplotlib.use('Agg')

    parser = argparse.ArgumentParser(prog='euler_gravity_simulation',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    # simulation settings
    parser.add_argument('-dt', '--time-step', type=float, default=0.1, help='time step (s)')
    parser.add_argument('-g', '--gravity', type=float, default=-9.80665, help='gravity (m/s^2)')
    parser.add_argument('-r', '--radius', type=float, default=0.2, help='sphere radius (m)')
    parser.add_argument('-m', '--mass', type=float, default=1.0, help='sphere mass (kg)')
    parser.add_argument('-rho', '--density', type=float, default=1.204, help='fluid density (kg/m^3)')
    parser.add_argument('-cd', '--drag', type=float, default=0.4, help='drag coefficient (-)')

    # initial conditions settings
    parser.add_argument('-v0', '--initial-velocity', type=float, default=0.0, help='initial velocity (m/s)')
    parser.add_argument('-y0', '--initial-position', type=float, default=10.0, help='initial height (m)')

    # steps
    parser.add_argument('-n', '--steps', type=int, default=80, help='number of simulation steps')

    # output
    parser.add_argument('-out', '--outfile', type=str, default='simulation.gif', help='simulation output file')
    parser.add_argument('-fdt', '--frame-duration', type=float, default=0.1, help='gif frame duration (s)')

    args = parser.parse_args()

    # run simulation
    settings = SimulationSettings(dt=args.time_step,
                                  g=args.gravity,
                                  r=args.radius,
                                  m=args.mass,
                                  rho=args.density,
                                  C_D=args.drag)

    initial_frame = Frame(t=0.0,
                          v=args.initial_velocity,
                          y=args.initial_position)

    simulation = Simulation(settings, initial_frame)
    simulation.simulate(args.steps)

    # create figure
    frames = plot_frames(simulation)
    gif_frames(frames, args.outfile, frame_duration=int(args.frame_duration * 1000))


if __name__ == '__main__':
    main()
