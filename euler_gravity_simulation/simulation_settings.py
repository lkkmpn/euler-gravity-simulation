from dataclasses import dataclass

import numpy as np


@dataclass
class SimulationSettings:
    dt: float  # time step (s)
    g: float  # gravity (m/s^2)
    r: float  # sphere radius (m)
    m: float  # sphere mass (kg)
    rho: float  # fluid density (kg/m^3)
    C_D: float  # drag coefficient (-)

    @property
    def A(self) -> float:
        return np.pi * self.r ** 2
