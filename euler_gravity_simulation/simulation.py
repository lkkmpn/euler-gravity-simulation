import numpy as np
from tqdm import tqdm

from .frame import Frame
from .simulation_settings import SimulationSettings


class Simulation:
    def __init__(self, settings: SimulationSettings, initial_frame: Frame):
        self.settings = settings
        self.frames: list[Frame] = [initial_frame]

    def simulate(self, n_frames: int) -> None:
        s = self.settings

        for _ in tqdm(range(n_frames), desc='Simulating'):
            f = self.frames[-1]
            F_g = s.m * s.g
            F_d = 0.5 * s.rho * s.C_D * s.A * f.v ** 2 * -np.sign(f.v)
            F = F_g + F_d
            a = F / s.m

            t = f.t + s.dt
            v = f.v + a * s.dt
            y = f.y + v * s.dt

            if y < s.r:  # bounce
                y = -y + 2 * s.r
                v = -v

            self.frames.append(Frame(t=t, v=v, y=y))
