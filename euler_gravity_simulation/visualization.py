import io

import matplotlib
import matplotlib.pyplot as plt
from matplotlib.axes import Axes
from matplotlib.figure import Figure
from matplotlib.patches import Circle
from PIL import Image
from tqdm import tqdm

from .simulation import Simulation


def plot_frame(simulation: Simulation, frame_index: int) -> Figure:
    s = simulation.settings
    frames = simulation.frames

    if frame_index < 0:
        frame_index = len(frames) + frame_index

    fig, axs = plt.subplots(1, 3, figsize=(12, 4), width_ratios=[1, 5, 5])

    ax_sim: Axes = axs[0]
    ax_y: Axes = axs[1]
    ax_v: Axes = axs[2]

    t_max = max(f.t for f in frames)
    v_min, v_max = min(f.v for f in frames), max(f.v for f in frames)
    y_max = max(f.y for f in frames)

    v_span = v_max - v_min

    frames = frames[:frame_index + 1]

    ts = [f.t for f in frames]
    ys = [f.y for f in frames]
    vs = [f.v for f in frames]

    circle = Circle((0, ys[-1]), s.r, fc='C0', ec='k')
    ax_sim.add_artist(circle)

    ax_sim.set_xlim(-s.r * 4, s.r * 4)
    ax_sim.set_ylim(0, y_max * 1.05)
    ax_sim.set_aspect('equal')

    ax_y.plot(ts, ys, 'o--')
    ax_y.set_xlim(0, t_max * 1.05)
    ax_y.set_ylim(0, y_max * 1.05)

    ax_v.plot(ts, vs, 'o--')
    ax_v.set_xlim(0, t_max * 1.05)
    ax_v.set_ylim(v_min - 0.05 * v_span, v_max + 0.05 * v_span)

    ax_sim.spines[['top', 'right']].set_visible(False)
    ax_sim.tick_params(bottom=False, labelbottom=False)
    ax_sim.set_ylabel(r'$y$ (m)')

    ax_y.set_xlabel(r'$t$ (s)')
    ax_y.set_ylabel(r'$y$ (m)')

    ax_v.set_xlabel(r'$t$ (s)')
    ax_v.set_ylabel(r'$v$ (m/s)')

    fig.tight_layout()

    return fig


def plot_frames(simulation: Simulation) -> list[Figure]:
    matplotlib.rcParams.update({'figure.max_open_warning': len(simulation.frames) + 1})

    figs: list[Figure] = []

    for i in tqdm(range(len(simulation.frames)), desc='Visualizing'):
        figs.append(plot_frame(simulation, i))

    return figs


def gif_frames(figures: list[Figure], outfile: str, frame_duration: int) -> None:
    images: list[Image.Image] = []

    pbar = tqdm(desc='Rendering', total=len(figures) + 1)

    for fig in figures:
        buf = io.BytesIO()
        fig.savefig(buf)
        buf.seek(0)
        images.append(Image.open(buf))
        pbar.update(1)

    images[0].save(outfile, save_all=True, append_images=images[1:], optimize=True, duration=frame_duration, loop=0)
    pbar.update(1)
    pbar.close()
