from setuptools import find_packages, setup

setup(
    name='euler-gravity-simulation',
    description='Simulate gravity using the Euler method',
    version='0.0.1',
    packages=find_packages(),
    install_requires=[
        'matplotlib',
        'numpy',
        'pillow',
        'tqdm'
    ],
    entry_points={
        'console_scripts': [
            'euler_gravity_simulation = euler_gravity_simulation.__main__:main'
        ]
    }
)
