# `euler-gravity-simulation`

Simple simulation and visualization of a ball bouncing on the ground using
Euler's method.

![Visualization of a simulation.](simulation.gif)

## Installation

Requires Python 3.9 or above.

Windows (with the Python launcher):
```bash
> git clone https://gitlab.com/lkkmpn/euler-gravity-simulation.git
> cd euler-gravity-simulation
> py -m venv env
> env\scripts\activate
> py -m pip install -e .
```

Linux:
```bash
$ git clone https://gitlab.com/lkkmpn/euler-gravity-simulation.git
$ cd euler-gravity-simulation
$ python3 -m venv env
$ source env/bin/activate
$ python3 -m pip install -e .
```

## Usage

Run with the default settings:
```bash
$ euler_gravity_simulation
```
This will create a file called `simulation.gif`.

Configure settings using command-line arguments:
```
usage: euler_gravity_simulation [-h] [-dt TIME_STEP] [-g GRAVITY] [-r RADIUS] [-m MASS] [-rho DENSITY] [-cd DRAG] [-v0 INITIAL_VELOCITY] [-y0 INITIAL_POSITION] [-n STEPS] [-out OUTFILE]

options:
  -h, --help            show this help message and exit
  -dt TIME_STEP, --time-step TIME_STEP
                        time step (s) (default: 0.1)
  -g GRAVITY, --gravity GRAVITY
                        gravity (m/s^2) (default: -9.80665)
  -r RADIUS, --radius RADIUS
                        sphere radius (m) (default: 0.2)
  -m MASS, --mass MASS  sphere mass (kg) (default: 1.0)
  -rho DENSITY, --density DENSITY
                        fluid density (kg/m^3) (default: 1.204)
  -cd DRAG, --drag DRAG
                        drag coefficient (-) (default: 0.4)
  -v0 INITIAL_VELOCITY, --initial-velocity INITIAL_VELOCITY
                        initial velocity (m/s) (default: 0.0)
  -y0 INITIAL_POSITION, --initial-position INITIAL_POSITION
                        initial height (m) (default: 10.0)
  -n STEPS, --steps STEPS
                        number of simulation steps (default: 80)
  -out OUTFILE, --outfile OUTFILE
                        simulation output file (default: simulation.gif)
  -fdt FRAME_DURATION, --frame-duration FRAME_DURATION
                        gif frame duration (s) (default: 0.1)
```

## License

This project is licensed under [the MIT license.](LICENSE.md)
